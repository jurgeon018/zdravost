





def blog(request):
    page_number = request.GET.get('page', 1)
    per_page = request.GET.get('per_page', 4)
    orderby = request.GET.get('order', 'title')
    posts = Post.objects.all().order_by(orderby)
    paginator = Paginator(posts, per_page=per_page)
    try: paginated_post = paginator.page(page_number)
    except PageNotAnInteger: paginated_posts = paginator.page(1)
    except EmptyPage: paginated_posts = paginator.page(paginator.num_pages)
    page = paginator.get_page(page_number)
    is_paginated = page.has_other_pages()
    next_url = f'?page={page.next_page_number()}' if page.has_next() else ''
    prev_url = f'?page={page.previous_page_number()}' if page.has_previous() else ''
    # next_url = f'?page={paginated_post.next_page_number()}' if paginated_post.has_next() else ''
    # prev_url = f'?page={paginated_post.previous_page_number()}' if paginated_post.has_previous() else ''
    most_recent = Post.objects.order_by('-created')[:3]
    category_count = get_category_count()
    tags = Tag.objects.all()
    context = {
        # 'posts':posts,
        'posts': paginated_post,
        'page':page,
        'is_paginated': is_paginated,
        'next_url':next_url or '',
        'prev_url':prev_url or '',
        'most_recent': most_recent,
        'category_count': category_count,
        'tags':tags,
        }
    
    # import pdb; pdb.set_trace()
    # print(request.GET)
    return render(request, 'blog/blog.html', context)






def cat_detail(request, id):
    cat = get_object_or_404(Category, id=id)
    orderby = request.GET.get('order', 'title')
    posts = Post.objects.all().filter(categories=cat).order_by(orderby)
    page_number = request.GET.get('page', 1)
    per_page = request.GET.get('per_page', 1)
    paginator = Paginator(posts, per_page=per_page)
    try: paginated_post = paginator.page(page_number)
    except PageNotAnInteger: paginated_posts = paginator.page(1)
    except EmptyPage: paginated_posts = paginator.page(paginator.num_pages)
    page = paginator.get_page(page_number)
    is_paginated = page.has_other_pages()
    next_url = f'?page={page.next_page_number()}' if page.has_next() else ''
    prev_url = f'?page={page.previous_page_number()}' if page.has_previous() else ''
    category_count = get_category_count()
    most_recent = Post.objects.order_by('-created')[:3]
    categories = Category.objects.all()
    tags = Tag.objects.all()
    title = f'All posts with category {cat.title}'
    return render(request, 'blog/blog.html', {
        # 'posts':posts,
        'title':title, 
        'categories': categories,
        'most_recent': most_recent,
        'category_count': category_count,
        'posts': paginated_post,
        'page':page,
        'is_paginated': is_paginated,
        'next_url':next_url or '',
        'prev_url':prev_url or '',
        'tags':tags,
        })


def tag_detail(request, id):
    tag = get_object_or_404(Tag, id=id)
    orderby = request.GET.get('order', 'title')
    posts = Post.objects.all().filter(tags=tag).order_by(orderby)
    page_number = request.GET.get('page', 1)
    per_page = request.GET.get('per_page', 1)
    paginator = Paginator(posts, per_page=per_page)
    try: paginated_post = paginator.page(page_number)
    except PageNotAnInteger: paginated_posts = paginator.page(1)
    except EmptyPage: paginated_posts = paginator.page(paginator.num_pages)
    page = paginator.get_page(page_number)
    is_paginated = page.has_other_pages()
    next_url = f'?page={page.next_page_number()}' if page.has_next() else ''
    prev_url = f'?page={page.previous_page_number()}' if page.has_previous() else ''
    category_count = get_category_count()
    most_recent = Post.objects.order_by('-created')[:3]
    categories = Category.objects.all()
    tags = Tag.objects.all()
    title = f'All posts with category {tag.title}'
    return render(request, 'blog/blog.html', {
        # 'posts':posts,
        'title':title, 
        'categories': categories,
        'most_recent': most_recent,
        'category_count': category_count,
        'posts': paginated_post,
        'page':page,
        'is_paginated': is_paginated,
        'next_url':next_url or '',
        'prev_url':prev_url or '',
        'tags':tags,
        })



def post_detail(request, id):
    post = get_object_or_404(Post, id=id)
    if request.user.is_authenticated:
        PostView.objects.get_or_create(user=request.user, post=post)
    category_count = get_category_count()
    most_recent = Post.objects.order_by('-created')[:3]
    categories = Category.objects.all()
    tags = Tag.objects.all()
    form = CommentForm(request.POST or None)
    for cat in categories:
        print((cat))
    if request.method == 'POST':
        if form.is_valid():
            print(form)
            form.instance.user = request.user
            form.instance.post = post
            form.save()
            return redirect(reverse('post_detail', kwargs={'id': post.id}))
    context = {
        'post':post,
        'categories': categories,
        'tags': tags,
        'most_recent': most_recent,
        'category_count': category_count,
        'form': form,
        'detail':True,
        'admin_object': post,

    }
    return render(request, 'blog/post.html', context)







# Для рассылки по e-mail надо настроить mailchimp или sendgrid

# MAILCHIMP_API_KEY = settings.MAILCHIMP_API_KEY
# MAILCHIMP_DATA_CENTER = settings.MAILCHIMP_DATA_CENTER
# MAILCHIMP_EMAIL_LIST_ID = settings.MAILCHIMP_EMAIL_LIST_ID


# api_url = 'https://{dc}.api.mailchimp.com/3.0'.format(dc=MAILCHIMP_DATA_CENTER)
# members_endpoint = '{api_url}/lists/{list_id}/members'.format(
#     api_url=api_url,
#     list_id=MAILCHIMP_EMAIL_LIST_ID
# )


# def subscribe(email):
#     data = {
#         "email_address": email,
#         "status": "subscribed"
#     }
#     r = requests.post(
#         members_endpoint,
#         auth=("", MAILCHIMP_API_KEY),
#         data=json.dumps(data)
#     )
#     return r.status_code, r.json()


# def email_list_signup(request):
#     form = EmailSignupForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             email_signup_qs = Signup.objects.filter(email=form.instance.email)
#             if email_signup_qs.exists():
#                 messages.info(request, "You are already subscribed")
#             else:
#                 subscribe(form.instance.email)
#                 form.save()
#     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))