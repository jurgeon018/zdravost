from django.test import TestCase
from blog.models import *


class PostTest(TestCase):
  
  def test_course_create(self):
    post1 = Post.objects.create(
      name='name1',
      description='desc1',
    )
    self.assertEqual(Post.objects.all().count(), 1)

  def test_pages(self):
    from django.test import Client
    client = Client()
    response = client.get('/post_detail/1/')  # , kwargs={'id':'1'})
    self.assertEqual(response.status_code, 404)
    post2 = Post.objects.create(
      name='name1', 
      description='desc1',
    )
    response = client.get('/post_detail/1/', kwargs={'id': 1})
    self.assertEqual(response.status_code, 200)
    self.assertContains(response, 'name1')
