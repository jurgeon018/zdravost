from django.db import models
from django.contrib.auth import get_user_model
from django.shortcuts import reverse 
from PIL import Image
from time import time
from django.utils.text import slugify
from django.utils import timezone
from tinymce.models import HTMLField


User = get_user_model()


class PostView(models.Model):
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  post = models.ForeignKey('Post', on_delete=models.CASCADE)
  def __str__(self):
    return self.user.username


class Signup(models.Model):
  email = models.EmailField(max_length=120)
  name = models.CharField(max_length=120)
  message = models.TextField()
  created = models.DateTimeField(auto_now_add=True)
  def __str__(self):
    return '{}'.format(self.email)


class Comment(models.Model):
  post = models.ForeignKey('Post', related_name='comments',on_delete=models.CASCADE)
  user = models.ForeignKey(User, on_delete=models.CASCADE)
  content = models.TextField()
  created = models.DateTimeField(auto_now_add=True, auto_now=False)
  updated = models.DateTimeField(auto_now_add=False, auto_now=True)
  def __str__(self):
      return self.user.username


class Category(models.Model):
  title = models.CharField(max_length=120)
  slug = models.SlugField(max_length=120, unique=True, null=True, blank=True)
  def save(self, *args, **kwargs):
    self.slug = 'slugify({}, allow_unicode=True)_str(int({}))'.format(self.title, self.id)
    super().save(*args, **kwargs)
  def __str__(self):
    return self.title
  def get_absolute_url(self):
    return reverse('cat_detail', kwargs={'pk':self.pk})


class Tag(models.Model):
  title = models.CharField(max_length=120)
  slug = models.SlugField(max_length=120, unique=True, null=True, blank=True)
  def save(self, *args, **kwargs):
    self.slug = 'slugify({}, allow_unicode=True)_str(int({}))'.format(self.title, self.id)
    super().save(*args, **kwargs)
  def __str__(self):
    return self.title
  def get_absolute_url(self):
    return reverse('tag_detail', kwargs={'pk':self.pk})


class Post(models.Model):
  title = models.CharField(max_length=120, blank=True, null=True)
  slug = models.SlugField(blank=True, null=True, max_length=120,unique=True)
  description = models.TextField(blank=True, null=True)
  created = models.DateTimeField(auto_now_add=True, auto_now=False)
  updated = models.DateTimeField(auto_now_add=False, auto_now=True)
  content = HTMLField('Content', blank=True, null=True)
  profile = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
  thumbnail = models.ImageField(blank=True, null=True, upload_to='post_pics')
  tags = models.ManyToManyField(Tag, blank=True, null=True)
  categories = models.ManyToManyField(Category, blank=True, null=True)
  featured = models.BooleanField(default=True)
  previous_post = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True, related_name='previous')
  next_post = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True, related_name='next')
  @property
  def get_comments(self):
    return self.comments.all().order_by('-created')
  # view_count = models.IntegerField(default=0)
  @property
  def view_count(self):
    return PostView.objects.filter(post=self).count()
  # comment_count = models.IntegerField(default=0)
  @property
  def comment_count(self):
    return Comment.objects.filter(post=self).count()
  def __str__(self): 
    return self.title
  def get_absolute_url(self):
    return reverse("post_detail", kwargs={"pk": self.pk})
  def get_delete_url(self):
    return reverse("post_delete", kwargs={"pk": self.pk})
  def get_update_url(self):
    return reverse("post_update", kwargs={"pk": self.pk})
  def save(self, *args, **kwargs):
    super().save(*args, **kwargs)
    try:
      img = Image.open(self.thumbnail.path)
      if img.height > 300 or img.width > 300:
        output_size = (300, 300)
        img.thumbnail(output_size)
        img.save(self.thumbnail.path)
    except:
      pass
    if not self.slug:
      self.slug = f'{slugify(self.title, allow_unicode=True)}_{self.id}'
    super().save(*args, **kwargs)


class Profile(models.Model):
  user = models.OneToOneField(User, on_delete=models.CASCADE)
  profile_picture = models.ImageField(upload_to='profile_pics', default='default.jpg')
  # friends = models.ManyToManyField('Profile', blank=True, null=True)
  def __str__(self):
    return 'Profile of '+self.user.username
  def save(self, *args, **kwargs):
    super().save(*args, **kwargs)
    img = Image.open(self.profile_picture.path)
    if img.height > 300 or img.width > 300:
      output_size = (300, 300)
      img.thumbnail(output_size)
      img.save(self.profile_picture.path)