
def get_category_count():
  posts = Post.objects\
              .values('categories__title', 'categories__pk')\
              .annotate(Count('categories__title'))
  return posts