from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin
from django.forms import widgets 
from django.db import models


class CommentInline(admin.StackedInline):
# class CommentInline(admin.TabularInline):
    model = Comment
    extra = 3

class PostAdmin(admin.ModelAdmin):
    list_per_page = 4
    # exclude = []
    # fields = ['title', 'slug', 'overview']
    fieldsets = [
        (None,                {'fields': ['title', 'slug']}),
        ('Other information', {'fields': ['content', 'profile', 'thumbnail', 'tags', 'categories', 'featured', 'previous_post', 'next_post'], 
                               'classes':['collapse']}),
    ]
    list_display = ['id','title', 'slug', 'profile', 'created', 'featured']
    list_display_links = ['id']#, 'title', 'profile', 'created']
    list_editable = ['title', 'slug','profile','featured']
    readonly_fields = ['featured']
    raw_id_fields = ['previous_post', 'next_post']
    list_filter = ['categories','tags', 'created', 'featured']
    search_fields = ['title', 'slug', 'content', 'tags__title', 'categories__title']
    # save_as = True # сохранить как новый обьект
    save_as = False # сохранить и добавить другой обьект
    save_on_top = True # добавляет панель для сохранения наверх
    formfield_overrides = {
        models.ManyToManyField: {'widget': widgets.CheckboxSelectMultiple},
        # models.DateTimeField: {'widget': widgets.TextInput}
    }
    inlines = [CommentInline]

# title
# slug
# overview
# created
# content
# profile
# thumbnail
# tags
# categories
# featured
# previous_post
# next_post

admin.site.register(Category)
admin.site.register(Profile)
admin.site.register(Signup)
admin.site.register(Comment)
admin.site.register(PostView)
admin.site.register(Tag)
admin.site.register(Post, PostAdmin)
# @admin.register(Post)
# class ViewAdmin(ImportExportModelAdmin):
#     pass
