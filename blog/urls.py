from django.urls import path
from blog.views import *
from django.views.decorators.cache import cache_page


urlpatterns = [

    path('', cache_page(60*15)(index), name = 'index'),

    path('search/', search, name = 'search'),

    path('blog/', blog, name='blog'),
    path('post_create/', post_create, name='post_create'),
    path('post_detail/<pk>/', post_detail, name='post_detail'),
    path('post_update/<pk>/', post_update, name='post_update'),
    path('post_delete/<pk>/', post_delete, name='post_delete'),

    # # PROFILES
    path('profile/', profile, name='profile'),
    path('profile/<pk>/', profile_pk, name='profile_pk'),
    path('profile/<pk>/delete/', profile_delete,name='profile_delete'),
    path('profile/<username>/posts/', profile_posts, name='profile_posts'),

    # # CRUD TAGS
    path('tags_list/', tags_list, name = 'tags_list'),
    path('tag_create/', tag_create, name = 'tag_create'),
    # path('tag/<pk>/detail/', tag_detail, name = 'tag_detail'),
    path('tag/<pk>/update/', tag_update, name = 'tag_update'),
    path('tag/<pk>/delete/', tag_delete, name = 'tag_delete'),
    # # CRUD CATEGORIES
    path('cats_list/', cats_list, name = 'cats_list'),
    path('cat_create/', cat_create, name = 'cat_create'),
    # path('cat/<pk>/detail/', cat_detail, name = 'cat_detail'),
    path('cat/<pk>/update/',cat_update, name = 'cat_update'),
    path('cat/<pk>/delete/',cat_delete,name = 'cat_delete'),
    # # CRUD POSTS
    path('blog/', blog, name = 'blog'),
    path('post_create/', post_create, name = 'post_create'),
    path('post/<pk>/detail/', post_detail, name = 'post_detail'),
    path('post/<pk>/update/',post_update, name = 'post_update'),
    path('post/<pk>/delete/',post_delete,name = 'post_delete'),

]
