import asyncio
import json
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from django.contrib.auth.models import User
from blog.models import *

class CommentConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print('connection successfull', event)
        # self.user = self.scope["user"]
        await self.send({
            'type': 'websocket.accept'
        })
        post_id = await self.get_post_id(
                        self.scope['url_route']['kwargs']['id']
                    )
        self.post = 'post_' + post_id
        print(self.post)
        print('******',self.channel_name)
        await self.channel_layer.group_add(
            self.post,
            self.channel_name
        )
        # await asyncio.sleep(5)
        # await self.send({
        #     'type': 'websocket.close',
        # })

        # await self.send({
        #     'type': 'websocket.send',
        #     'text': 'sdfsdf'
        # })
    async def websocket_receive(self, event):
        print('received', event)
        new_comment = json.loads(event.get('text'))
        print((event))
        print('new_comment********',new_comment)
        id = new_comment['id']
        user = new_comment['user']
        content = new_comment['content']
        await self.create_comment(id , user, content)
        comment = {
            'content': content,
            'user': user
        }
        await self.channel_layer.group_send(
            self.post,
            {
                'type': 'show_comment',
                'text': json.dumps(comment)
            }
        )

    @database_sync_to_async
    def create_comment(self, id, user, content):
        user = User.objects.get(username=user)
        post = Post.objects.get(id=id)
        Comment.objects.create(post=post,
                               user=user,
                               content=content)
    @database_sync_to_async
    def get_post_id(self, id):
        return str(Post.objects.get(id=id).id)

    async def show_comment(self, event):
        await self.send({
            'type': 'websocket.send',
            'text': event['text']
        })
