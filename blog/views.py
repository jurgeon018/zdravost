from django.shortcuts import render, redirect, reverse, get_object_or_404
from blog.forms import *
from django.contrib import messages
from blog.models import *
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import path, reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count, Q
from .models import *
from .forms import *
from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404, HttpResponseForbidden, HttpResponseNotFound
from django.core.exceptions import PermissionDenied
import json
import requests
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import send_mail
from django.template.loader import get_template
import logging
from django.contrib.auth import get_user_model
from blog.utils import *

User = get_user_model()
# logger = logging.getLogger(__name__) 


@login_required
def profile(request):
    p_form = ProfileForm(request.POST or None, request.FILES or None, instance = request.user.profile)
    u_form = UserForm(request.POST or None, instance = request.user)
    if request.method == 'POST' and u_form.is_valid() and p_form.is_valid():
        u_form.save()
        p_form.save()
        messages.success(request, 'Ваши данные были обновлены')
        return redirect(reverse('profile'))
    return render(request, 'blog/profile.html', locals())


def profile_pk(request, pk):
  profile = Profile.objects.get(pk=pk)
  return render(request, 'blog/profile_pk.html', {'profile':profile})


@login_required
def profile_delete(request, pk):
  if int(pk) == request.user.profile.pk:
    profile = get_object_or_404(Profile, pk=int(pk))
    user = get_object_or_404(User, pk=int(pk))
    profile.delete()
    user.delete()
    messages.success(request, 'Ваши аккаунт был удален')
    request.session.flush()
    return redirect('blog')
  messages.success(request, 'Неверные данные')
  return redirect('blog')


def profile_posts(request, username):
  user = User.objects.get(username=username)
  posts = Post.objects.all().filter(profile__pk=user.pk)
  for post in posts:
      print(post.content)
  return render(request, 'blog/profile_posts.html', {'posts':posts})


def index(request):
  ''' тестовая работа с почтой, потом надо будет подключить sendgrid или mailchimp'''
  # logger.debug('debug info!')
  if request.method == 'POST':
    name = request.POST['name']
    email = request.POST['email']
    message = request.POST['message']
    subject = ' Contact form Received '
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = [settings.DEFAULT_FROM_EMAIL, email]
    new_signup = Signup()
    new_signup.name = name
    new_signup.email = email
    new_signup.message = message
    new_signup.save()
    context = {
        'user': name,
        'email': email,
        'message': message,
    } 
    contact_message = get_template('blog/contact_message.txt').render(context)
    # contact_message = '{}, from {} with email {}'.format(message, name,email)
    send_mail(subject, contact_message, from_email, to_email, fail_silently=True)
    return redirect(reverse('index'))
  return render(request, 'blog/index.html', {})


def search(request):
  posts = Post.objects.all()
  query = request.GET.get('q')
  if query:
    posts = posts.filter(
      Q(title__icontains=query) |
      Q(overview__icontains=query)
    ).distinct()
  context = {
    'posts': posts
  }
  template = 'blog/search_results.html'
  return render(request, template, context)


# posts CRUD

def blog(request):
  posts = Post.objects.all()
  return render(request, 'blog/blog.html', locals())


@login_required
def post_create(request):
  title = "Создать статью"
  form = PostForm(request.POST or None, request.FILES or None)
  if request.method == 'POST':
    if form.is_valid():
      form.instance.profile = request.user
      form.save()    
      messages.success(request, 'Статья была успешно создана!')
      return redirect('post_detail', pk=form.instance.id)
  return render(request, 'blog/create_or_update.html', locals())


def post_detail(request, pk):
  post = get_object_or_404(Post, pk=pk)
  return render(request, 'blog/post.html', locals())


@login_required
def post_update(request, pk):
  title = 'Изменить Статью'
  post = get_object_or_404(Post, pk=pk)
  if request.user != post.profile:
    raise PermissionDenied
  form = PostForm(
    request.POST or None, 
    request.FILES or None,
    instance=post
  )
  if request.method == 'POST':
    if form.is_valid():
      form.instance.profile = request.user
      form.save()
      messages.success(request, 'Статья была успешно изменена!')
      return redirect('post_detail', pk=form.instance.pk) #pk=post.pk)
  return render(request, 'blog/create_or_update.html', locals())


@login_required
def post_delete(request, pk):
  post = get_object_or_404(Post, pk=pk)
  if request.method == 'POST':
    if request.user != post.profile:
      return HttpResponseForbidden()      
    post.delete()
    messages.success(request, 'Статья была успешно удалена!')
    return redirect('posts_list')  
  return render(request, 'blog/post_delete.html', locals())  


# Categories CRUD

def cats_list(request):
  cats = Category.objects.all()
  return  render(request, 'blog/cats_list.html',  {'cats':cats})

@staff_member_required
def cat_create(request):
  form = CatForm(request.POST or None)
  if request.method == 'POST' and form.is_valid():
    form.save()
    return redirect(reverse('cat_detail', kwargs={'id':form.instance.id}))
  return render(request, 'blog/create_or_update.html', {'form':form})

  
@staff_member_required
def cat_update(request, pk):
  title = 'Изменить категорию'
  cat = get_object_or_404(Category, id=id)
  form = TagForm(request.POST or None, instance=cat)
  # form = TagForm(request.POST or None, {'title': cat.title, 
  #                                       'slug': cat.slug})
  if request.method == 'POST':
      if form.is_valid():
          form.save()
          return redirect(reverse('cat_detail', kwargs={'id':form.instance.id}))
  return render(request, 'blog/create_or_update.html', {'form':form,'title': title})


@staff_member_required
def cat_delete(request, pk):
    cat = get_object_or_404(Category, pk=pk)
    cat.delete()
    return redirect(reverse('blog'))
    # return render(request, 'blog/delete.html', {})

    
# Tags CRUD

def tags_list(request):
  tags = Tag.objects.all()
  return  render(request, 'blog/tags_list.html', {'tags':tags})


@staff_member_required
def tag_create(request):
  title = 'Создать новый тэг'
  form = TagForm(request.POST or None)
  if request.method == 'POST':
    if form.is_valid():
      form.save()
      return redirect(reverse('tag_detail', kwargs={'id':form.instance.id}))
  return render(request, 'blog/create_or_update.html', {'form':form, 'title':title})


@staff_member_required
def tag_update(request, pk):
  title = 'Изменить тэг'
  tag = get_object_or_404(Tag, pk=pk)
  form = TagForm(request.POST or None, instance=tag) #{'title': tag.title, 'slug': tag.slug})
  if request.method == 'POST':
      if form.is_valid():
          form.save()
          return redirect(reverse('tag_detail', kwargs={'pk':form.instance.pk}))
  return render(request, 'blog/create_or_update.html', {'form':form,'title': title})


@staff_member_required
def tag_delete(request, pk):
    tag = get_object_or_404(Tag, pk=pk)
    tag.delete()
    return redirect(reverse('blog'))
    # return render(request, 'blog/delete.html', {})