from django import forms
from blog.models import *
from tinymce.widgets import TinyMCE


class TinyMCEWidget(TinyMCE):
  def use_required_attribute(self, *args, **kwargs):
    return False


class ProfileForm(forms.ModelForm):
  class Meta:
    model = Profile
    fields = ['profile_picture']


class TagForm(forms.ModelForm):
  class Meta:
    model = Tag
    fields = ['title', 'slug']
    widgets = {
        'title': forms.TextInput(attrs={'class':'form-control'}),
        'slug': forms.TextInput(attrs={'class': 'form-control'}),
    }


class CatForm(forms.ModelForm):
  class Meta:
    model = Category
    fields = ['title', 'slug']
    widgets = {
        'title': forms.TextInput(attrs={'class':'form-control'}),
        'slug': forms.TextInput(attrs={'class': 'form-control'}),
    }

    
class PostForm(forms.ModelForm):
  content = forms.CharField(
    widget=TinyMCEWidget(
      attrs={'required': False, 'cols': 30, 'rows': 10},
    )
  )
  class Meta:
    model = Post
    # fields = ['title', 'slug', 'overview', 'content', 'thumbnail', 'categories', 'tags','featured', 'previous_post', 'next_post', ]
    fields = '__all__'
    widgets = {
      'title': forms.TextInput(attrs={'class':'form-control'}),
      'slug': forms.TextInput(attrs={'class': 'form-control'}),
      'overview': forms.Textarea(attrs={'class':'form-control'}),
      'tags': forms.SelectMultiple(attrs={'class':'form-control'}),
      'categories': forms.SelectMultiple(attrs={'class':'form-control'}),
    }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']
    content = forms.CharField(widget=forms.Textarea(attrs={
      'class': 'form-control',
      'id': 'id_content',
      'placeholder': 'Введите ваш коментарий...',
      'rows': '4'
    })) # создаст это: <div id="<textarea name="usercomment" id="usercomment" placeholder="Type your comment" class="form-control"></textarea>"></div>


class EmailSignupForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.TextInput(attrs={
        "type": "email",
        "name": "email",
        "id": "email",
        "placeholder": "Type your email address",
    }), label="")
    name = forms.CharField(max_length=120)
    message = forms.Textarea()

    class Meta:
        model = Signup
        fields = ('email', 'name', 'message')




class UserForm(forms.ModelForm):
  class Meta:
      model = User
      fields = ['username', 'email', 'first_name', 'last_name',]
  email = forms.EmailField(required=False)





        











        
