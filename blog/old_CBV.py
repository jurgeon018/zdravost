from django.shortcuts import render, get_object_or_404, redirect
from django.urls import path, reverse, reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count, Q
from .models import *
from .forms import *
from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404, HttpResponseForbidden, HttpResponseNotFound
from django.core.exceptions import PermissionDenied
import json
import requests
from django.views.generic import View
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import send_mail
from django.template.loader import get_template
from django.contrib.auth.views import LoginView, LogoutView, PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.views.decorators.cache import cache_page
from django.contrib.auth import get_user_model
User = get_user_model()
import logging
logger = logging.getLogger(__name__) # app1.views


##########################################################################################################################################
#LIST
class CatsList(ListView):
    model = Category
    template_name = 'justdjango/cats_list.html'
    context_object_name = 'cats'
class TagsList(ListView):
    model = Tag
    template_name = 'justdjango/tags_list.html'
    context_object_name = 'tags'
class PostsList(ListView):
    model = Post
    template_name = 'justdjango/blog.html'
    context_object_name = 'posts'
    paginate_by = 4
    def get_context_data(self, **kwargs):
        page_number = self.request.GET.get('page', 1)
        per_page = self.request.GET.get('per_page', 4)
        orderby = self.request.GET.get('order', 'title')
        posts = Post.objects.all().order_by(orderby)
        paginator = Paginator(posts, per_page=per_page)
        try: paginated_post = paginator.page(page_number)
        except PageNotAnInteger: paginated_posts = paginator.page(1)
        except EmptyPage: paginated_posts = paginator.page(paginator.num_pages)
        page = paginator.get_page(page_number)
        is_paginated = page.has_other_pages()
        next_url = f'?page={page.next_page_number()}' if page.has_next() else ''
        prev_url = f'?page={page.previous_page_number()}' if page.has_previous() else ''
        context = super().get_context_data(**kwargs)
        context['posts'] = paginated_post
        context['page'] = page
        context['is_paginated'] = is_paginated
        context['next_url'] = next_url
        context['prev_url'] = prev_url
        context['most_recent'] = Post.objects.order_by('-created')[:3]
        context['caregory_count'] = get_category_count()
        context['tags'] = Tag.objects.all()
        return context
##########################################################################################################################################
#CREATE
class CatCreate(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Category
    form_class = CatForm
    template_name = 'justdjango/create_or_update.html'
    success_url = reverse_lazy('blog')
    def form_valid(self, form):
        messages.success('Category created')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user.is_staff:
            return True
        return False
class TagCreate(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Tag
    form_class = TagForm
    template_name = 'justdjango/create_or_update.html'
    success_url = reverse_lazy('blog')
    def form_valid(self, form):
        messages.success('Tag created')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user.is_staff:
            return True
        return False
class PostCreate(LoginRequiredMixin, CreateView):
    model = Post
    form_class = PostForm
    template_name = 'justdjango/create_or_update.html'
    success_url = reverse_lazy('cat_detail')
    def form_valid(self, form):
        messages.success('Post created')
        return super().form_valid(form)
##########################################################################################################################################
#READ  
class CatDetail(View):
    model = Category
    template_name = 'justdjango/blog.html'
    def get(self, request, pk):
        cat = get_object_or_404(self.model, pk=pk)
        orderby = request.GET.get('order', 'title')
        context = {}
        posts = Post.objects.all().filter(categories=cat).order_by(orderby)
        page_number = self.request.GET.get('page', 1)
        per_page = self.request.GET.get('per_page', 1)
        paginator = Paginator(posts, per_page=per_page)
        try: paginated_post = paginator.page(page_number)
        except PageNotAnInteger: paginated_posts = paginator.page(1)
        except EmptyPage: paginated_posts = paginator.page(paginator.num_pages)
        page = paginator.get_page(page_number)
        is_paginated = page.has_other_pages()
        next_url = f'?page={page.next_page_number()}' if page.has_next() else ''
        prev_url = f'?page={page.previous_page_number()}' if page.has_previous() else ''
        category_count = get_category_count()
        most_recent = Post.objects.order_by('-created')[:3]
        categories = Category.objects.all()
        tags = Tag.objects.all()
        title = f'All posts with category {cat.title}'
        context['orderby'] = orderby
        context['title'] = title
        context['categories'] = categories
        context['most_recent'] = most_recent
        context['category_count'] = category_count
        context['posts'] = paginated_post
        context['page'] = page
        context['is_paginated'] = is_paginated
        context['next_url'] = next_url
        context['prev_url'] = prev_url
        context['tags'] = tags
        return render(request, self.template_name, context)

class TagDetail(View):
    model = Tag
    template_name = 'justdjango/blog.html'
    def get(self, request, pk):
        tag = get_object_or_404(Tag, pk=pk)
        orderby = request.GET.get('order', 'title')
        posts = Post.objects.all().filter(tags=tag).order_by(orderby)
        page_number = request.GET.get('page', 1)
        per_page = request.GET.get('per_page', 1)
        paginator = Paginator(posts, per_page=per_page)
        try: paginated_post = paginator.page(page_number)
        except PageNotAnInteger: paginated_posts = paginator.page(1)
        except EmptyPage: paginated_posts = paginator.page(paginator.num_pages)
        page = paginator.get_page(page_number)
        is_paginated = page.has_other_pages()
        next_url = f'?page={page.next_page_number()}' if page.has_next() else ''
        prev_url = f'?page={page.previous_page_number()}' if page.has_previous() else ''
        category_count = get_category_count()
        most_recent = Post.objects.order_by('-created')[:3]
        categories = Category.objects.all()
        tags = Tag.objects.all()
        title = f'All posts with category {tag.title}'
        return render(request, 'justdjango/blog.html', {
            # 'posts':posts,
            'title':title, 
            'categories': categories,
            'most_recent': most_recent,
            'category_count': category_count,
            'posts': paginated_post,
            'page':page,
            'is_paginated': is_paginated,
            'next_url':next_url or '',
            'prev_url':prev_url or '',
            'tags':tags,
            })

class PostDetail(View):
    model = Post
    template_name = 'justdjango/blog.html'
    def get(self, request, pk):
        post = get_object_or_404(Post, pk=pk)
        if request.user.is_authenticated:
            PostView.objects.get_or_create(user=request.user, post=post)
        category_count = get_category_count()
        most_recent = Post.objects.order_by('-created')[:3]
        categories = Category.objects.all()
        tags = Tag.objects.all()
        form = CommentForm(request.POST or None)
        for cat in categories:
            print((cat))
        if request.method == 'POST':
            if form.is_valid():
                print(form)
                form.instance.user = request.user
                form.instance.post = post
                form.save()
                return redirect(reverse('post_detail', kwargs={'pk': post.pk}))
        context = {
            'post':post,
            'categories': categories,
            'tags': tags,
            'most_recent': most_recent,
            'category_count': category_count,
            'form': form,
            'detail':True,
            'admin_object': post,

        }
        return render(request, self.template_name, context)

##########################################################################################################################################
#UPDATE
class CatUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Category
    form_class = CatForm
    template_name = 'justdjango/create_or_update.html'
    # success_url = reverse_lazy('cat_detail')#
    # не обязательно, если определен get_avbsolute_url

    def form_valid(self, form):
        messages.success(self.request, 'Категория обновлена')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user.is_staff:
            return True
        return False
class TagUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Tag
    form_class = TagForm
    template_name = 'justdjango/create_or_update.html'
    success_url = reverse_lazy('tag_detail')
    def form_valid(self, form):
        messages.success(self.request, 'Тэг обновлен')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user.is_staff:
            return True
        return False
class PostUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    form_class = PostForm
    template_name = 'justdjango/create_or_update.html'
    success_url = reverse_lazy('post_detail')
    def form_valid(self, form):
        messages.success(self.request, 'Категория обновлена')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user == self.get_object().profile:
            return True
        return False
##########################################################################################################################################
#DELETE   
class CatDelete(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Category
    template_name = 'justdjango/delete.html'
    success_url=reverse_lazy('blog')
    def test_func(self):
        if self.request.user.is_staff:
            return True
        return False
class TagDelete(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Tag
    template_name = 'justdjango/delete.html'
    success_url=reverse_lazy('blog')
    def test_func(self):
        if self.request.user.is_staff:
            return True
        return False
class PostDelete(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    template_name = 'justdjango/delete.html'
    success_url=reverse_lazy('blog')
    def test_func(self):
        if self.request.user == self.get_object().profile or self. request.user.is_staff:
            return True
        return False
##########################################################################################################################################
# PROFILES AND AUTHENTICATION

class Register(FormView):
    template_name='justdjango/register.html'
    success_url = reverse_lazy('login')
    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Account has been created')

class ProfileId(DetailView):
    model = Profile
    template_name='justdjango/profile_id.html'


@login_required
def profile(request):
    p_form = ProfileForm(request.POST or None, request.FILES or None, instance = request.user.profile)
    u_form = UserForm(request.POST or None, instance = request.user)
    if request.method == 'POST' and u_form.is_valid() and p_form.is_valid():
        u_form.save()
        p_form.save()
        messages.success(request, 'Your account has been updated')
        return redirect(reverse('profile'))
    return render(request, 'justdjango/profile.html', {'u_form':u_form, 'p_form': p_form})


class ProfilePosts(View):
    model = Post
    template_name = 'justdjango/profile_posts.html'
    def get(self, request, username):
        user = User.objects.get(username=username)
        posts = Post.objects.all().filter(profile_id=user.pk)
        return render(request, self.template_name, {'posts':posts})
##########################################################################################################################################

# MAILCHIMP_API_KEY = settings.MAILCHIMP_API_KEY
# MAILCHIMP_DATA_CENTER = settings.MAILCHIMP_DATA_CENTER
# MAILCHIMP_EMAIL_LIST_ID = settings.MAILCHIMP_EMAIL_LIST_ID

# api_url = 'https://{dc}.api.mailchimp.com/3.0'.format(dc=MAILCHIMP_DATA_CENTER)
# members_endpoint = '{api_url}/lists/{list_id}/members'.format(
#     api_url=api_url,
#     list_id=MAILCHIMP_EMAIL_LIST_ID
# )


# def subscribe(email):
#     data = {
#         "email_address": email,
#         "status": "subscribed"
#     }
#     r = requests.post(
#         members_endpoint,
#         auth=("", MAILCHIMP_API_KEY),
#         data=json.dumps(data)
#     )
#     return r.status_code, r.json()


# def email_list_signup(request):
#     form = EmailSignupForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             email_signup_qs = Signup.objects.filter(email=form.instance.email)
#             if email_signup_qs.exists():
#                 messages.info(request, "You are already subscribed")
#             else:
#                 subscribe(form.instance.email)
#                 form.save()
#     return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
##########################################################################################################################################
def search(request):
    posts = Post.objects.all()
    query = request.GET.get('q')
    if query:
        posts = posts.filter(
            Q(title__icontains=query) |
            Q(overview__icontains=query)
        ).distinct()
    context = {
        'posts': posts
    }
    template = 'justdjango/search_results.html'
    return render(request, template, context)


def index(request):
    logger.debug('debug info!')
    featured = Post.objects.filter(featured=True)
    latest = Post.objects.order_by('-created')[0:3]
    template = 'justdjango/index.html'
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        message = request.POST['message']
        subject = ' Contact form Received '
        from_email = settings.DEFAULT_FROM_EMAIL
        to_email = [settings.DEFAULT_FROM_EMAIL, email]
        new_signup = Signup()
        new_signup.name = name
        new_signup.email = email
        new_signup.message = message
        new_signup.save()
        context = {
            'user': name,
            'email': email,
            'message': message,
        } 
        contact_message = get_template('justdjango/contact_message.txt').render(context)
        # contact_message = '{}, from {} with email {}'.format(message, name,email)
        send_mail(subject, contact_message, from_email, to_email, fail_silently=True)
        return redirect(reverse('index'))

    context = {
        'featured': featured,
        'latest': latest
    }
    return render(request, template, context)


def get_category_count():
    posts = Post.objects\
                .values('categories__title', 'categories__pk')\
                .annotate(Count('categories__title'))
    return posts
##########################################################################################################################################
urlpatterns  =  [
    path('', cache_page(60*15)(index), name = 'index'),
    path('search/', search, name = 'search'),
    # CRUD TAGS
    path('tags_list/', TagsList.as_view(), name = 'tags_list'),
    path('tag_create/', TagCreate.as_view(), name = 'tag_create'),
    path('tag/<pk>/detail/', TagDetail.as_view(), name = 'tag_detail'),
    path('tag/<pk>/update/', TagUpdate.as_view(), name = 'tag_update'),
    path('tag/<pk>/delete/', TagDelete.as_view(), name = 'tag_delete'),
    # CRUD CATEGORIES
    path('cats_list/', CatsList.as_view(), name = 'cats_list'),
    path('cat_create/', CatCreate.as_view(), name = 'cat_create'),
    path('cat/<pk>/detail/', CatDetail.as_view(), name = 'cat_detail'),
    path('cat/<pk>/update/',CatUpdate.as_view(), name = 'cat_update'),
    path('cat/<pk>/delete/',CatDelete.as_view(),name = 'cat_delete'),
    # CRUD POSTS
    path('blog/', PostsList.as_view(), name = 'blog'),
    path('blog/', PostsList.as_view(), name = 'j_blog'),
    path('post_create/', PostCreate.as_view(), name = 'post_create'),
    path('post/<pk>/detail/', PostDetail.as_view(), name = 'post_detail'),
    path('post/<pk>/update/',PostUpdate.as_view(), name = 'post_update'),
    path('post/<pk>/delete/',PostDelete.as_view(),name = 'post_delete'),
    # PROFILES
    path('profile/', profile, name = 'profile'),
    path('profile/<pk>/', ProfileId.as_view(), name='profile_id'),
    path('profile/<username>/posts/', ProfilePosts.as_view(), name='profile_posts'),
    path('register/', Register.as_view(), name = 'register'),    
    path('login/', LoginView.as_view(
                        template_name = 'justdjango/login.html'
                    ), 
        name = 'login'),    
    path('logout/', LogoutView.as_view(
                        template_name = 'justdjango/logout.html'
                    ), 
        name = 'j_logout'),
    path('password-reset/',
         PasswordResetView.as_view(
             template_name='justdjango/password_reset.html',
            #  email_template_name='justdjango/password_reset_email.html',
            #  subject_template_name='justdjango/password_reset_subject.txt',
         ),
         name='password_reset'),
    path('password-reset/done/',
         PasswordResetDoneView.as_view(
             template_name='justdjango/password_reset_done.html'
         ),
         name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/',
         PasswordResetConfirmView.as_view(
             template_name='justdjango/password_reset_confirm.html'
         ),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         PasswordResetCompleteView.as_view(
             template_name='justdjango/password_reset_complete.html'
         ),
         name='password_reset_complete'),
]
