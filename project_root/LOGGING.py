import os
from .base import BASE_DIR
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'loggers':{
        'app1.views':{
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
        },
        'app2.models':{
            'handlers': ['console', 'file2'],
            'level': 'DEBUG',
        },
        'app3.urls':{
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    },
    'handlers':{
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
        },
        'file':{
            'level': 'DEBUG',
            # 'level': 'INFO',
            # 'level': 'WARNING',
            # 'level': 'ERROR',
            # 'level': 'CRITICAL',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'test_debug_1.log'),
            # 'filename': os.path.join(BASE_DIR, 'debug.log'),
            'formatter':'verbose',
        },
        'file2':{
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'test_debug_2.log'),
            # 'filename': os.path.join(BASE_DIR, 'debug_student.log'),
            'formatter':'simple',
        },
        # 'mail_admins':{
        #     'level':'ERROR',
        #     'filters':['require_debug_false'],
        #     'class':'django.utils.log.AdminEmailHandler',
        # }
    },
    'formatters':{
        'verbose':{
            'format':'%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple':{
            'format':'%(levelname)s %(message)s'
        },
    },
}