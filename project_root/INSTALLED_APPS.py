INSTALLED_APPS = [
    # 'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    # Third-Party Apps
    # 'channels',
    'crispy_forms',
    'notifications',
    'tinymce',
    'filebrowser',
    'import_export',
    'rest_framework',
    # 'stripe',
    'django_countries', 
    'django_extensions', # python3 manage.py help
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    # 'allauth.socialaccount.providers.google',
    # 'allauth.socialaccount.providers.facebook',
    # 'allauth.socialaccount.providers.vk',
    # 'allauth.socialaccount.providers.disqus',
    # 'allauth.socialaccount.providers.github',
    # 'allauth.socialaccount.providers.gitlab',

    # Local Apps
    'core.apps.CoreConfig',
    'blog.apps.BlogConfig',
    'custom_auth',
    'friends',
]