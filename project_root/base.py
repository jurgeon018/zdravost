import os
from decouple import config
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# SECRET_KEY = config('SECRET_KEY')
SECRET_KEY='98o4o_2*c@*u+xjnpm93d*-cs(f!0tzwb2repgr@ev)2&gx(pu'
ROOT_URLCONF = 'project_root.urls'
ADMINS = (('SomeAdmin', 'jurgeon020@gmail.com'), ('Andrew', 'jurgeon018@gmail.com'))
WSGI_APPLICATION = 'project_root.wsgi.application'
# ASGI_APPLICATION = 'project_root.routing.application'
LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'Europe/Kiev'
USE_I18N = True
USE_L10N = True
USE_TZ = True
VENV_PATH = os.path.dirname(BASE_DIR)
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static_root') 
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media') 
STATICFILES_DIRS = [os.path.join(BASE_DIR, "static"),]
LOGIN_REDIRECT_URL = 'profile'
LOGIN_URL = 'login'
CRISPY_TEMPLATE_PACK = 'bootstrap4'


from .TEMPLATES import *
from .EMAIL import *
from .INSTALLED_APPS import *
from .MIDDLEWARE import *
from .LOGGING import *
from .CACHES import *
from .TINYMCE import *
from .ALLAUTH import *
from .REST import *
from .CHANNEL_LAYERS import *
from .AUTH_PASSWORD_VALIDATORS import *