from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView
from filebrowser.sites import site
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


User = get_user_model()


app_name = 'base'

urlpatterns = [
    path('', include('blog.urls')),
    path('', include('custom_auth.urls')),
    path('', include('friends.urls')),
    # Third-party apps
    path('admin/', admin.site.urls),
    path('grappelli/', include('grappelli.urls')),
    path('admin/filebrowser/', site.urls),
    path('tinymce/', include('tinymce.urls')),
    path('api/', include('rest_framework.urls')),
    path('accounts/', include('allauth.urls')),
    # path('accounts/profile/', TemplateView.as_view(template_name='profile.html')),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)