# zdravost


    virtual environment
python3 -m venv venv
source venv/bin/activate
deactivate
pip freeze > requirements.txt # for deploy
pip install -r requirements.txt # for first usage
  


  git
https://github.com/jurgeon018/zdravost.git
echo "# zdravost" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/jurgeon018/zdravost.git
git push -u origin master


  heroku
heroku git:remote -a zdravost
git push heroku master
#only for postgre
#heroku run python manage.py makemigrations  
#heroku run python manage.py migrate  
#heroku run python manage.py createsuperuser


  ~/.bashrc or ~/.profile snippets
alias shell='python manage.py shell'
alias r='python manage.py runserver'
alias mm='python manage.py makemigrations'
alias m='python manage.py migrate'
alias csu='python manage.py createsuperuser'
alias req='pip install -r requirements.txt'
alias 8001='python manage.py runserver 8001'
alias 8002='python manage.py runserver 8002'
alias 8003='python manage.py runserver 8003'
alias bashrc='. ~/.bashrc'
. ~/.bashrc


  bootstrap link
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  current issues
почему то не заливает на гитхаб папку media, при том что ее нет в .gitignore