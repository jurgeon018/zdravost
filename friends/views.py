from django.shortcuts import render, redirect, reverse, get_object_or_404
from blog.models import Profile


def users_list(request):
	users = Profile.objects.exclude(user=request.user)
	return render(request, 'users_list.html', {'users':users})

def send_frequest(request, pk):
	print(request.user)
	if request.user.is_authenticated:
		user = get_object_or_404(User, pk=pk)
		frequest, created = FriendRequest.objects.get_or_create(
			from_user=request.user,
			to_user=user)
		return redirect('/blog/')

def cancel_frequest(request, pk):
	if request.user.is_authenticated:
		user = get_object_or_404(User, pk=pk)
		frequest = FriendRequest.objects.filter(
			from_user=request.user,
			to_user=user).first()
		frequest.delete()
		return redirect('/users')

def accept_frequest(request, pk):
	from_user = get_object_or_404(User, pk=pk)
	frequest = FriendRequest.objects.filter(from_user=from_user, to_user=request.user).first()
	user1 = frequest.to_user
	user2 = from_user
	user1.profile.friends.add(user2.profile)
	user2.profile.friends.add(user1.profile)
	frequest.delete()
	return redirect('/users/{}'.format(request.user.profile.slug))

def delete_frequest(request, pk):
	from_user = get_object_or_404(User, pk=pk)
	frequest = FriendRequest.objects.filter(from_user=from_user, to_user=request.user).first()
	frequest.delete()
	return redirect('/blog/users_list/{}'.format(request.user.profile.slug))
