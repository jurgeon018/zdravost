

from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class FriendRequest(models.Model):
  to_user = models.ForeignKey(User, related_name='to_user', on_delete=models.CASCADE)
  from_user = models.ForeignKey(User, related_name='from_user', on_delete=models.CASCADE)
  created = models.DateTimeField(auto_now_add=True)
  def __str__(self):
      return f'From {self.from_user_username} to {self.to_user.username}'
