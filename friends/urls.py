from django.urls import path
from friends.views import *


urlpatterns = [
    path('users_list/', users_list, name='users_list'),
    path('send_frequest/<pk>/', send_frequest, name='send_frequest'),
    path('cancel_frequest/<pk>/', cancel_frequest, name='cancel_frequest'),
    path('accept_frequest/<pk>/', accept_frequest, name='accept_frequest'),
    path('delete_frequest/<pk>/', delete_frequest, name='delete_frequest'),
]
