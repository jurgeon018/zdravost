from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.views.decorators.cache import cache_page
from custom_auth.forms import *
from custom_auth.models import *
from django.contrib import messages
import logging

logger = logging.getLogger(__name__) # app1.views

@cache_page(60 * 15)
def register(request):
    logger.debug('debug info!')
    form = RegisterForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        form.save()
        messages.success(request, 'Account has been created')
        return redirect(reverse('login'))
    return render(request, 'custom_auth/register.html', {'form':form})
