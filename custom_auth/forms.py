from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth import get_user_model

User = get_user_model()


class RegisterForm(UserCreationForm):
  class Meta:
      model = User
      fields = ['username','password1','password2','email']
      # exclude = ['field_name1', 'field_name2',]
      # widgets = {'field_name': forms.RadioSelect,}
      labels = {'username': 'имя пользователя', 'email':'Имейл'}
  email = forms.EmailField()