from django.urls import path
from custom_auth.views import *
from django.contrib.auth.views import (
  LoginView, LogoutView, PasswordResetView, 
  PasswordResetDoneView, PasswordResetConfirmView, 
  PasswordResetCompleteView
)


urlpatterns = [
  path('register/', register, name = 'register'),    
  path('login/', LoginView.as_view(
                      template_name = 'custom_auth/login.html'
                  ), 
      name = 'login'),    
  path('logout/', LogoutView.as_view(
    template_name = 'custom_auth/logout.html'), name = 'logout'),
  path('password-reset/',
        PasswordResetView.as_view(
            template_name='custom_auth/password_reset.html',
          #  email_template_name='custom_auth/password_reset_email.html',
          #  subject_template_name='custom_auth/password_reset_subject.txt',
        ), name='password_reset'),
  path('password-reset/done/',
    PasswordResetDoneView.as_view(
      template_name='custom_auth/password_reset_done.html'), 
      name='password_reset_done'),
  path('password-reset-confirm/<uidb64>/<token>/',
    PasswordResetConfirmView.as_view(
      template_name='custom_auth/password_reset_confirm.html'), 
      name='password_reset_confirm'),
  path('password-reset-complete/',
    PasswordResetCompleteView.as_view(
      template_name='custom_auth/password_reset_complete.html'), 
      name='password_reset_complete'),
]


