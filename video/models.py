from django.db import models

# https://docs.djangoproject.com/en/2.2/ref/files/storage/#django.core.files.storage.FileSystemStorage

# https://docs.djangoproject.com/en/2.2/ref/models/fields/

class Video(models.Model):
  video1 = models.FileField()
  video2 = models.FieldFile()
  video3 = models.FilePathField()
  video4 = models.URLField()
  created = models.DateTimeField(auto_now=False, auto_now_add=True)
  updated = models.DateTimeField(auto_now=True, auto_now_add=False)