from django.shortcuts import render, redirect, reverse, get_object_or_404


def index(request):
  return render(request, 'video/index.html', locals())